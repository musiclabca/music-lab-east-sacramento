Our East Sacramento facility is a four teaching room facility tucked away in the beautiful neighborhood known as “River Park”. This location started out as our second location of Drum Lab but we now offer guitar, bass guitar, piano, vocal, cello and other instruments as well as vocal lessons.

Address: 5495 Carlson Dr, Suite C, Sacramento, CA 95819, USA

Phone: 916-616-5762
